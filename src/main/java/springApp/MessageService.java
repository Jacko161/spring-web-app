package springApp;

import org.springframework.stereotype.Service;

@Service
public class MessageService {
	
	public MessageService(){
		
	}
	
	public String updateMessage(String message){
		message += " The Bean Works ";
		return message;
	}

}
